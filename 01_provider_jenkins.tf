provider "google" {
	credentials = "${file("terraform_jenkins_key.json")}"
	project = "terraform-jenkins-214214"
	region = "europe-west2"
}
